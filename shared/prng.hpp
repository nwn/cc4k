// Pseudo Random Number Generator : generate repeatable sequence of values that
//   appear random by scrambling the bits of a 32-bit integer value.
//
// Interface :
//   PRNG(s) - set starting seed
//   seed() - read seed
//   seed(s) - reset seed
//   prng() - generate random value in range [0,UINT_MAX]
//   prng(l,u) - generate random value in range [l,u]
//
// Examples : generate random number between 5-21
//   prng() % 17 + 5	values 0-16 + 5 = 5-21
//   prng(16) + 5
//   prng(5, 21)

#ifndef PRNG_HPP
#define PRNG_HPP

#include "util.hpp"
#include <cassert> // assert

class PRNG {
    uint32  _seed; // same results on 32/64-bit architectures
public:
    PRNG(uint32 s = 362436069) {
    	_seed = s; // set seed
    	assert(((void)"invalid seed", _seed != 0));
    }
    uint32 seed() { // read seed
        return _seed;
    }
    void seed(uint32 s) { // reset seed
    	_seed = s; // set seed
    	assert(((void)"invalid seed", _seed != 0));
    }
    uint32 operator()() { // [0,UINT_MAX]
    	_seed = 36969 * (_seed & 65535) + (_seed >> 16); // scramble bits
    	return _seed;
    }
    uint32 operator()(uint32 l, uint32 u) { // [l,u]
    	assert(((void)"invalid random range", l <= u));
    	return operator()() % (u - l + 1) + l; // call operator()(uint32_t)
    }
};

#endif // PRNG_HPP
