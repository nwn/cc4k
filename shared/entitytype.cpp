#include "entitytype.hpp"

EntityType EntityTypes::Null = 0;
EntityType EntityTypes::Character = 1 << 0;
EntityType EntityTypes::Player = (1 << 1) & EntityTypes::Character;
EntityType EntityTypes::Item = 0 << 0;
