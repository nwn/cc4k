#ifndef ENTITYSTATE_HPP
#define ENTITYSTATE_HPP

#include "util.hpp"
#include "entitytype.hpp"

struct EntityState {
    EntityID id;
    EntityType type;
    Coord coord;
};

#endif // ENTITYSTATE_HPP
