#ifndef FLOORSTATE_HPP
#define FLOORSTATE_HPP

#include "cellstate.hpp"
#include "packet.hpp"
#include <vector>

struct FloorState {
    std::vector<std::vector<CellState>> cells;
    std::size_t depth;
};

static Packet& operator<<(Packet& packet, const FloorState& floor_state) {
    packet << floor_state.cells << floor_state.depth;
    return packet;
}

static Packet& operator>>(Packet& packet, FloorState& floor_state) {
    packet >> floor_state.cells >> floor_state.depth;
    return packet;
}

#endif // FLOORSTATE_HPP
