#ifndef CHARACTERSTATE_HPP
#define CHARACTERSTATE_HPP

#include "util.hpp"
#include "entitystate.hpp"
#include "packet.hpp"

struct CharacterState : public EntityState {
    uint16 hp, max_hp;
    uint16 atk, def;
};

static Packet& operator<<(Packet& packet, const CharacterState& character_state) {
    packet << character_state.id << character_state.type << character_state.coord.row << character_state.coord.col << character_state.coord.level;
    packet << character_state.hp << character_state.max_hp << character_state.atk << character_state.def;
    return packet;
}

static Packet& operator>>(Packet& packet, CharacterState& character_state) {
    packet >> character_state.id >> character_state.type >> character_state.coord.row >> character_state.coord.col >> character_state.coord.level;
    packet >> character_state.hp >> character_state.max_hp >> character_state.atk >> character_state.def;
    return packet;
}

#endif // CHARACTERSTATE_HPP
