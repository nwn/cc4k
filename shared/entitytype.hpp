#ifndef ENTITYTYPE_HPP
#define ENTITYTYPE_HPP

#include "util.hpp"

typedef uint32 EntityID;

typedef uint8 EntityType;
namespace EntityTypes {
    extern EntityType Null;
    extern EntityType Character;
    extern EntityType Player;
    extern EntityType Item;
}

#endif // ENTITYTYPE_HPP
