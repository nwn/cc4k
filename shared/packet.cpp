#include "packet.hpp"
#include <cstring>

std::size_t Packet::size() const {
    return data.size();
}
void Packet::clear() {
    data.clear();
    read_pos = 0;
    write_pos = 0;
}

bool Packet::pack(ENetPacket* packet) const {
    std::size_t original_packet_length = packet->dataLength;
    if(enet_packet_resize(packet, original_packet_length + data.size()) != 0) {
        return false;
    }
    std::memcpy(&packet->data[original_packet_length], data.data(), data.size());
    return true;
}
void Packet::unpack(ENetPacket* packet) {
    data.resize(packet->dataLength);
    read_pos = 0;
    write_pos = 0;
    std::memcpy(data.data(), packet->data, data.size());
}

Packet& Packet::operator<<(const int8 data) {
    this->data.resize(this->data.size() + 1);
    this->data.at(write_pos++) = data;
    return *this;
}
Packet& Packet::operator<<(const uint8 data) {
    operator<<(static_cast<int8>(data));
    return *this;
}
Packet& Packet::operator<<(const int16 data) {
    const uint8 data_size = 2;
    for(uint8 offset = data_size; offset-- > 0;) {
        int8 byte = 0;
        byte |= (data >> (8 * offset));
        operator<<(byte);
    }
    return *this;
}
Packet& Packet::operator<<(const uint16 data) {
    const uint8 data_size = 2;
    for(uint8 offset = data_size; offset-- > 0;) {
        int8 byte = 0;
        byte |= (data >> (8 * offset));
        operator<<(byte);
    }
    return *this;
}
Packet& Packet::operator<<(const int32 data) {
    const uint8 data_size = 4;
    for(uint8 offset = data_size; offset-- > 0;) {
        int8 byte = 0;
        byte |= (data >> (8 * offset));
        operator<<(byte);
    }
    return *this;
}
Packet& Packet::operator<<(const uint32 data) {
    const uint8 data_size = 4;
    for(uint8 offset = data_size; offset-- > 0;) {
        int8 byte = 0;
        byte |= (data >> (8 * offset));
        operator<<(byte);
    }
    return *this;
}
Packet& Packet::operator<<(const int64 data) {
    const uint8 data_size = 8;
    for(uint8 offset = data_size; offset-- > 0;) {
        int8 byte = 0;
        byte |= (data >> (8 * offset));
        operator<<(byte);
    }
    return *this;
}
Packet& Packet::operator<<(const uint64 data) {
    const uint8 data_size = 8;
    for(uint8 offset = data_size; offset-- > 0;) {
        int8 byte = 0;
        byte |= (data >> (8 * offset));
        operator<<(byte);
    }
    return *this;
}

Packet& Packet::operator>>(int8& data) {
    data = this->data.at(read_pos++);
    return *this;
}
Packet& Packet::operator>>(uint8& data) {
    int8 byte;
    operator>>(byte);
    data = static_cast<uint8>(byte);
    return *this;
}
Packet& Packet::operator>>(int16& data) {
    const uint8 data_size = 2;
    data = 0;
    for(uint8 offset = data_size; offset-- > 0;) {
        int8 byte;
        operator>>(byte);
        data <<= 8;
        data |= byte;
    }
    return *this;
}
Packet& Packet::operator>>(uint16& data) {
    const uint8 data_size = 2;
    data = 0;
    for(uint8 offset = data_size; offset-- > 0;) {
        int8 byte;
        operator>>(byte);
        data <<= 8;
        data |= byte;
    }
    return *this;
}
Packet& Packet::operator>>(int32& data) {
    const uint8 data_size = 4;
    data = 0;
    for(uint8 offset = data_size; offset-- > 0;) {
        int8 byte;
        operator>>(byte);
        data <<= 8;
        data |= byte;
    }
    return *this;
}
Packet& Packet::operator>>(uint32& data) {
    const uint8 data_size = 4;
    data = 0;
    for(uint8 offset = data_size; offset-- > 0;) {
        int8 byte;
        operator>>(byte);
        data <<= 8;
        data |= byte;
    }
    return *this;
}
Packet& Packet::operator>>(int64& data) {
    const uint8 data_size = 8;
    data = 0;
    for(uint8 offset = data_size; offset-- > 0;) {
        int8 byte;
        operator>>(byte);
        data <<= 8;
        data |= byte;
    }
    return *this;
}
Packet& Packet::operator>>(uint64& data) {
    const uint8 data_size = 8;
    data = 0;
    for(uint8 offset = data_size; offset-- > 0;) {
        int8 byte;
        operator>>(byte);
        data <<= 8;
        data |= byte;
    }
    return *this;
}

Packet& operator<<(Packet& pd, const std::string& data) {
    pd << static_cast<uint32>(data.size());
    for(std::size_t i = 0; i < data.size(); ++i) {
        pd << static_cast<int8>(data[i]);
    }
    return pd;
}
Packet& operator>>(Packet& pd, std::string& data) {
    uint32 size;
    pd >> size;
    data.resize(size);
    for(std::size_t i = 0; i < data.size(); ++i) {
        int8 byte;
        pd >> byte;
        data[i] = static_cast<char>(byte);
    }
    return pd;
}
