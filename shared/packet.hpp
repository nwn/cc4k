#ifndef PACKET_HPP
#define PACKET_HPP

#include "util.hpp"
#include <enet/enet.h>
#include <vector>
#include <string>

struct Packet {
private:
    std::vector<int8> data;
    std::size_t read_pos = 0;
    std::size_t write_pos = 0;
public:
    std::size_t size() const;
    void clear();

    bool pack(ENetPacket* enet_packet) const;
    void unpack(ENetPacket* enet_packet);

    Packet& operator<<(const int8 data);
    Packet& operator<<(const uint8 data);
    Packet& operator<<(const int16 data);
    Packet& operator<<(const uint16 data);
    Packet& operator<<(const int32 data);
    Packet& operator<<(const uint32 data);
    Packet& operator<<(const int64 data);
    Packet& operator<<(const uint64 data);

    Packet& operator>>(int8& data);
    Packet& operator>>(uint8& data);
    Packet& operator>>(int16& data);
    Packet& operator>>(uint16& data);
    Packet& operator>>(int32& data);
    Packet& operator>>(uint32& data);
    Packet& operator>>(int64& data);
    Packet& operator>>(uint64& data);
};

template <typename T>
Packet& operator<<(Packet& packet, const std::vector<T>& data) {
    packet << static_cast<uint32>(data.size());
    for(std::size_t i = 0; i < data.size(); ++i) {
        packet << data[i];
    }
    return packet;
}
template <typename T>
Packet& operator>>(Packet& packet, std::vector<T>& data) {
    uint32 size;
    packet >> size;
    data.resize(size);
    for(std::size_t i = 0; i < data.size(); ++i) {
        packet >> data[i];
    }
    return packet;
}

Packet& operator<<(Packet& packet, const std::string& data);
Packet& operator>>(Packet& packet, std::string& data);

#endif // PACKET_HPP
