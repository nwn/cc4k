#ifndef GAMESTATE_HPP
#define GAMESTATE_HPP

#include "floorstate.hpp"
#include "packet.hpp"
#include <vector>

struct CharacterState;

struct GameState {
    FloorState floor;
    std::size_t player_index;
    std::vector<CharacterState> players;
    std::vector<CharacterState> non_players;
};

static Packet& operator<<(Packet& packet, const GameState& game_state) {
    packet << game_state.floor << game_state.player_index << game_state.players << game_state.non_players;
    return packet;
}

static Packet& operator>>(Packet& packet, GameState& game_state) {
    packet >> game_state.floor >> game_state.player_index >> game_state.players >> game_state.non_players;
    return packet;
}

#endif // GAMESTATE_HPP
