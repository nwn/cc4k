#ifndef CELLSTATE_HPP
#define CELLSTATE_HPP

#include "util.hpp"
#include "packet.hpp"

struct CellState {
    enum class Type : uint8 {
        Void, Wall, Floor
    } type;

    uint32 entity_id;
};

static Packet& operator<<(Packet& packet, const CellState& cell_state) {
    packet << static_cast<uint8>(cell_state.type) << cell_state.entity_id;
    return packet;
}

static Packet& operator>>(Packet& packet, CellState& cell_state) {
    uint8 type;
    packet >> type >> cell_state.entity_id;
    cell_state.type = static_cast<CellState::Type>(type);
    return packet;
}

#endif // CELLSTATE_HPP
