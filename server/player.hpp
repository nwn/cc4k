#ifndef PLAYER_HPP
#define PLAYER_HPP

#include "character.hpp"

struct Player : public Character {
    virtual ~Player() = default;

    virtual EntityType type() const override;
};

#endif // PLAYER_HPP
