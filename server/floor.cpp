#include "floor.hpp"
#include "floorstate.hpp"
#include "cellstate.hpp"
#include "character.hpp"
#include "characterstate.hpp"
#include "player.hpp"
#include "cell.hpp"
#include "prng.hpp"

Floor::~Floor() {
    /*for(std::size_t i = 0; i < items.size(); ++i) {
        delete items[i];
    }*/
    for(std::size_t i = 0; i < enemies.size(); ++i) {
        delete enemies[i];
    }
}

void Floor::initialize(std::size_t depth, PRNG& prng) {
    this->depth = depth;
    cells.resize(24);
    for(std::size_t i = 0; i < cells.size(); ++i) {
        cells[i].resize(72);
        for(std::size_t j = 0; j < cells[i].size(); ++j) {
            cells[i][j].coord.row = i;
            cells[i][j].coord.col = j;
            cells[i][j].coord.level = this->depth;
        }
    }
}

void Floor::place_player(Player* const player, PRNG& prng) {
    Coord coord;
    coord.level = depth;
    do {
        coord.row = prng(0, cells.size() - 1);
        coord.col = prng(0, cells[coord.row].size() - 1);
    } while(cells[coord.row][coord.col].entity != nullptr);
    cells[coord.row][coord.col].entity = player;
    player->cell = &cells[coord.row][coord.col];
}

FloorState Floor::state() const {
    FloorState state;

    state.cells.resize(cells.size());
    for(std::size_t i = 0; i < state.cells.size(); ++i) {
        state.cells[i].resize(cells[i].size());
        for(std::size_t j = 0; j < state.cells[i].size(); ++j) {
            state.cells[i][j] = cells[i][j].state();
        }
    }

    state.depth = depth;

    return state;
}
