#ifndef SERVER_HPP
#define SERVER_HPP

#include "util.hpp"
#include <enet/enet.h>
#include <vector>
#include <functional>
#include <string>

struct Packet;

class Server {
public:
    typedef ENetPeer* Peer;
    typedef std::function<void(Server&, const Peer&, uint32)> ConnectionCallback;
    typedef std::function<void(Server&, const Peer&, Packet&, uint8)> PacketCallback;
    typedef std::function<void(Server&, const Peer&, uint32)> DisconnectionCallback;

    ConnectionCallback connection_callback;
    PacketCallback packet_callback;
    DisconnectionCallback disconnection_callback;
private:
    ENetHost* local = nullptr;
    std::vector<ENetPeer*> remotes;
public:
    Server(const uint32 host_name, const uint16 host_port, const std::size_t peer_limit = 32, const std::size_t channel_limit = 2);
    Server(const uint16 host_port, const std::size_t peer_limit = 32, const std::size_t channel_limit = 2);
    ~Server();

    void update();

    bool connect(const std::string& remote_host_name, const uint16 remote_port, const uint32 timeout = 1000);
    bool send(const Peer& peer, const Packet& packet, const uint8 channel) const;
    bool disconnect(const Peer& peer, const uint16 data = 0);

    bool connected(const Peer& peer) const;
};

#endif // SERVER_HPP
