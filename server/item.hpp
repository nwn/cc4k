#ifndef ITEM_HPP
#define ITEM_HPP

#include "entity.hpp"

namespace EntityTypes {
    EntityType Item = 0 << 0;
}

struct Item : public Entity {};

#endif ITEM_HPP
