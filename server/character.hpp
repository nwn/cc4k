#ifndef CHARACTER_HPP
#define CHARACTER_HPP

#include "entity.hpp"
#include "util.hpp"

struct CharacterState;

struct Character : public Entity {
    virtual ~Character() = default;

    uint16 hp, max_hp;
    uint16 atk, def;

    virtual EntityType type() const override;

    CharacterState state() const;
};

#endif // CHARACTER_HPP
