#ifndef CELL_HPP
#define CELL_HPP

#include "util.hpp"

struct CellState;
struct Entity;

struct Cell {
    enum class Type {
        Void, Wall, Floor
    } type = Type::Floor;

    Coord coord;
    Entity* entity = nullptr;

    CellState state() const;
};

#endif // CELL_HPP
