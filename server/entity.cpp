#include "entity.hpp"

EntityID Entity::last_id = 0;

Entity::Entity() : id(++last_id) {}
EntityType Entity::type() const {
    return EntityTypes::Null;
}
