#include "cell.hpp"
#include "cellstate.hpp"
#include "entity.hpp"

CellState Cell::state() const {
    CellState state;

    if(type == Type::Void) {
        state.type = CellState::Type::Void;
    } else if(type == Type::Wall) {
        state.type = CellState::Type::Wall;
    } else if(type == Type::Floor) {
        state.type = CellState::Type::Floor;
    }

    if(entity != nullptr) state.entity_id = entity->id;
    else state.entity_id = 0;

    return state;
}
