#include "server.hpp"
#include "packet.hpp"
#include <stdexcept>

Server::Server(const uint32 host_name, const uint16 host_port, const std::size_t peer_limit, const std::size_t channel_limit):
    connection_callback([] (Server&, const Peer&, uint32) {}),
    packet_callback([] (Server&, const Peer&, Packet&, uint8) {}),
    disconnection_callback([] (Server&, const Peer&, uint32) {}) {
    ENetAddress host_address = {.host = host_name, .port = host_port};
    local = enet_host_create(&host_address, peer_limit, channel_limit, 0, 0);
    if(local == nullptr) throw std::runtime_error("Failure to create ENet host.");
}
Server::Server(const uint16 host_port, const std::size_t peer_limit, const std::size_t channel_limit):
    Server(ENET_HOST_ANY, host_port, peer_limit, channel_limit) {}

Server::~Server() {
    for(std::size_t i = 0; i < remotes.size(); ++i) {
        disconnect(remotes[i]);
    }
    enet_host_destroy(local);
}

void Server::update() {
    // poll events
    ENetEvent event;
    while(enet_host_service(local, &event, 0) > 0) {
        if(event.type == ENET_EVENT_TYPE_CONNECT) {
            remotes.push_back(event.peer);
            connection_callback(*this, event.peer, event.data);
        } else if(event.type == ENET_EVENT_TYPE_RECEIVE) {
            Packet packet;
            packet.unpack(event.packet);
            enet_packet_destroy(event.packet);
            packet_callback(*this, event.peer, packet, event.channelID);
        } else if(event.type == ENET_EVENT_TYPE_DISCONNECT) {
            disconnection_callback(*this, event.peer, event.data);
            for(std::size_t i = 0; i < remotes.size(); ++i) {
                if(remotes[i] == event.peer) {
                    remotes.erase(remotes.begin() + i);
                }
            }
        }
    }
}

bool Server::send(const Peer& peer, const Packet& packet, const uint8 channel) const {
    if(!connected(peer)) return false;

    ENetPacket* enet_packet = enet_packet_create(nullptr, 0, ENET_PACKET_FLAG_RELIABLE);
    packet.pack(enet_packet);
    if(enet_peer_send(peer, channel, enet_packet) != 0) {
        return false;
    }
    enet_host_flush(local);
    return true;
}

bool Server::disconnect(const Peer& peer, const uint16 data) {
    if(!connected(peer)) return false;

    // force disconnection
    enet_peer_disconnect_now(peer, data);

    // remove peer
    for(std::size_t i = 0; i < remotes.size(); ++i) {
        if(remotes[i] == peer) {
            remotes.erase(remotes.begin() + i);
            break;
        }
    }

    return true;
}

bool Server::connected(const Server::Peer& peer) const {
    // search for peer in remotes
    for(std::size_t i = 0; i < remotes.size(); ++i) {
        if(remotes[i] == peer) return true;
    }
    return false;
}
