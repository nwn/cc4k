#ifndef ENTITY_HPP
#define ENTITY_HPP

#include "util.hpp"
#include "entitystate.hpp"
#include <string>

struct Cell;

struct Entity {
private:
    static EntityID last_id;
public:
    Entity();
    virtual ~Entity() = default;
    virtual EntityType type() const;

    const EntityID id;
    Cell* cell;
    std::string name;
};

#endif // ENTITY_HPP
