#include "character.hpp"
#include "characterstate.hpp"
#include "cell.hpp"

EntityType Character::type() const {
    return EntityTypes::Character;
}

CharacterState Character::state() const {
    CharacterState state;

    state.id = id;
    state.coord = cell->coord;
    state.hp = hp;
    state.max_hp = max_hp;
    state.atk = atk;
    state.def = def;

    return state;
}
