#include "game.hpp"
#include "floor.hpp"
#include "player.hpp"
#include "gamestate.hpp"
#include "floorstate.hpp"
#include "characterstate.hpp"

Game::Game() : prng(12) {
    players.emplace_back(new Player);
    players.back()->hp = 100;
    players.back()->max_hp = 100;
    players.back()->atk = 20;
    players.back()->def = 10;
    floors.emplace_back(new Floor);
    floors.back()->initialize(0, prng);
    floors.back()->place_player(players.back(), prng);
}

Game::~Game() {
    for(std::size_t i = 0; i < floors.size(); ++i) {
        delete floors[i];
    }
    for(std::size_t i = 0; i < players.size(); ++i) {
        delete players[i];
    }
}

GameState Game::state(const EntityID& entity_id) const {
    GameState state;

    state.players.resize(players.size());
    for(std::size_t i = 0; i < state.players.size(); ++i) {
        state.players[i] = players[i]->state();
        if(players[i]->id == entity_id) state.player_index = i;
        state.floor = floors[state.players[i].coord.level]->state();
    }

    return state;
}
