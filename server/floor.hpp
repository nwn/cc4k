#ifndef FLOOR_HPP
#define FLOOR_HPP

#include "cell.hpp"
#include <vector>

struct Character;
struct Player;
struct PRNG;
struct FloorState;

struct Floor {
    std::vector<std::vector<Cell>> cells;
    std::vector<Character*> enemies;
    // std::vector<Item*> items;
    std::size_t depth;

    Floor() = default;
    ~Floor();
    // no copying
    Floor(const Floor&) = delete;
    Floor& operator=(const Floor&) = delete;

    void initialize(std::size_t depth, PRNG& prng);
    void place_player(Player* const player, PRNG& prng);
    FloorState state() const;
};

#endif // FLOOR_HPP
