#ifndef GAME_HPP
#define GAME_HPP

#include "prng.hpp"
#include "entitytype.hpp"
#include <vector>

struct Floor;
struct Player;
struct GameState;

class Game {
    std::vector<Floor*> floors;
    std::vector<Player*> players;
    PRNG prng;

public:
    Game();
    ~Game();

    GameState state(const EntityID& entity_id) const;
};

#endif // GAME_HPP
