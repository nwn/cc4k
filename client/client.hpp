#ifndef CLIENT_HPP
#define CLIENT_HPP

#include "util.hpp"
#include <enet/enet.h>
#include <functional>
#include <string>

struct Packet;

class Client {
public:
    typedef ENetPeer* Peer;
    typedef std::function<void(Client&, const Peer&, uint32)> ConnectionCallback;
    typedef std::function<void(Client&, const Peer&, Packet&, uint8)> PacketCallback;
    typedef std::function<void(Client&, const Peer&, uint32)> DisconnectionCallback;

    ConnectionCallback connection_callback;
    PacketCallback packet_callback;
    DisconnectionCallback disconnection_callback;
private:
    ENetHost* local = nullptr;
    ENetPeer* remote = nullptr;
public:
    Client(const std::size_t peer_limit = 1, const std::size_t channel_limit = 2);
    ~Client();

    void update();

    bool connect(const std::string& remote_host_name, const uint16 remote_port, const uint32 timeout = 1000);
    bool send(const Packet& packet, const uint8 channel) const;
    bool disconnect(const uint16 data = 0);

    bool connected() const;
};

#endif // CLIENT_HPP
