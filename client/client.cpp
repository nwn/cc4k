#include "client.hpp"
#include "packet.hpp"
#include <stdexcept>

Client::Client(const std::size_t peer_limit, const std::size_t channel_limit):
    connection_callback([] (Client&, const Peer&, uint32) {}),
    packet_callback([] (Client&, const Peer&, Packet&, uint8) {}),
    disconnection_callback([] (Client&, const Peer&, uint32) {}) {
    local = enet_host_create(nullptr, peer_limit, channel_limit, 0, 0);
    if(local == nullptr) throw std::runtime_error("Failure to create ENet host.");
}

Client::~Client() {
    if(connected()) disconnect();
    enet_host_destroy(local);
}

void Client::update() {
    // poll events
    ENetEvent event;
    while(enet_host_service(local, &event, 0) > 0) {
        if(event.type == ENET_EVENT_TYPE_CONNECT) {
            if(!connected()) remote = event.peer;
            connection_callback(*this, event.peer, event.data);
        } else if(event.type == ENET_EVENT_TYPE_RECEIVE) {
            Packet packet;
            packet.unpack(event.packet);
            enet_packet_destroy(event.packet);
            packet_callback(*this, event.peer, packet, event.channelID);
        } else if(event.type == ENET_EVENT_TYPE_DISCONNECT) {
            disconnection_callback(*this, event.peer, event.data);
            remote = nullptr;
        }
    }
}

bool Client::connect(const std::string& remote_host_name, const uint16 remote_port, const uint32 timeout) {
    if(connected()) disconnect(); // disconnect from previous

    // resolve remote host address
    ENetAddress remote_address;
    enet_address_set_host(&remote_address, remote_host_name.c_str());
    remote_address.port = remote_port;

    // initiate connection
    remote = enet_host_connect(local, &remote_address, 2, 0);
    if(remote == nullptr) {
        return false;
    }

    // await connection confirmation
    ENetEvent event;
    if(enet_host_service(local, &event, timeout) > 0 && event.type == ENET_EVENT_TYPE_CONNECT) {
        return true;
    } else {
        enet_peer_reset(remote);
        remote = nullptr;
        return false;
    }
}

bool Client::send(const Packet& packet, const uint8 channel) const {
    if(!connected()) return false;

    ENetPacket* enet_packet = enet_packet_create(nullptr, 0, ENET_PACKET_FLAG_RELIABLE);
    packet.pack(enet_packet);
    if(enet_peer_send(remote, channel, enet_packet) != 0) {
        return false;
    }
    enet_host_flush(local);
    return true;
}

bool Client::disconnect(const uint16 data) {
    if(!connected()) return false; // already disconnected

    // force disconnection
    enet_peer_disconnect_now(remote, data);
    remote = nullptr;
    return true;
}

bool Client::connected() const {
    return (remote != nullptr);
}
