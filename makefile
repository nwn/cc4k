# Universal makefile for multiple C++ programs
#
# Use gcc flag -MMD (user) or -MD (user/system) to generate dependences among source files.
# Use make default rules for commonly used file-name suffixes and make variables names.
#
# % make [ client | server ]

########## Variables ##########

INC = -Iclient -Iserver -Ishared
LIB = -lncursesw -lenet

CXX = g++					# compiler
CXXFLAGS = -g -std=c++11 -Wall -Wextra -MMD ${INC} ${LIB}		# compiler flags
MAKEFILE_NAME = ${firstword ${MAKEFILE_LIST}}	# makefile name

OBJECTS1 = game-client.o client/client.o shared/packet.o shared/entitytype.o	# object files forming 1st executable
TARGET1 = client
EXEC1 = ${TARGET1}.out						# 1st executable name

OBJECTS2 = game-test.o server/game.o server/floor.o server/cell.o server/entity.o server/character.o server/player.o shared/entitytype.o				# object files forming 2nd executable
TARGET2 = server
EXEC2 = ${TARGET2}.out						# 2nd executable name

OBJECTS = ${OBJECTS1} ${OBJECTS2}	# all object files
DEPENDS = ${OBJECTS:.o=.d}			# substitute ".o" with ".d"
TARGETS = ${TARGET1} ${TARGET2}		# all targets
EXECS = ${EXEC1} ${EXEC2}			# all executables

########## Targets ##########

.PHONY : all clean				# not file names

all : ${TARGETS}					# build all targets

${TARGET1} : ${OBJECTS1}					# link step 1st executable
	${CXX} ${CXXFLAGS} $^ -o ${EXEC1}	# additional object files before $^

${TARGET2} : ${OBJECTS2}					# link step 2nd executable
	${CXX} ${CXXFLAGS} $^ -o ${EXEC2}	# additional object files before $^

${OBJECTS} : ${MAKEFILE_NAME}			# OPTIONAL : changes to this file => recompile

-include ${DEPENDS}				# include *.d files containing program dependences

clean :						# remove files that can be regenerated
	rm -f ${DEPENDS} ${OBJECTS} ${EXECS}
