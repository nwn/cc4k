#include "client.hpp"
#include "packet.hpp"
#include "gamestate.hpp"
#include "characterstate.hpp"
#include "cellstate.hpp"
#include <ncursesw/ncurses.h>
#include <fstream>

std::ofstream log("game-client.log");

void render(WINDOW* win, Packet& packet) {
    log << "Rendering packet..." << std::endl;
    GameState game_state;
    packet >> game_state;

    for(std::size_t i = 0; i < game_state.floor.cells.size(); ++i) {
        for(std::size_t j = 0; j < game_state.floor.cells[i].size(); ++j) {
            const CellState& cs = game_state.floor.cells[i][j];

            // determine display character
            chtype c;
            if(cs.entity_id != 0) {
                bool found = false;
                for(std::size_t i = 0; i < game_state.players.size(); ++i) {
                    if(game_state.players[i].id == cs.entity_id) {
                        c = '@' | A_BOLD;
                        if(i == game_state.player_index) c |= COLOR_PAIR(1);
                        else c |= COLOR_PAIR(i % 6 + 2);
                        found = true;
                        break;
                    }
                }
                if(!found) log << "Our hero has gone missing; the world will surely perish." << std::endl;
            }
            else if(cs.type == CellState::Type::Void) c = ' ';
            else if(cs.type == CellState::Type::Wall) c = '#';
            else if(cs.type == CellState::Type::Floor) c = '.';
            else c = '?';

            mvwaddch(win, i, j, c);
        }
    }
    wrefresh(win);
}

void handle_packet(WINDOW* win, Client&, const Client::Peer&, Packet& packet, uint8 channel) {
    if(channel == 0) {
        render(win, packet);
    }
}

static void run_client() {
    Client client;

    WINDOW* main = newwin(0, 0, 0, 0); // fullscreen window

    using namespace std::placeholders;
    client.packet_callback = std::bind(handle_packet, main, _1, _2, _3, _4);

    while(true) {
        const int input = wgetch(main);
        if(input == 27) break;

        client.update();
    }
    delwin(main);
}

int main() {
    // initialize ncurses
    initscr(); // initialize curses mode
    cbreak(); // exit cooked mode
    noecho(); // do not echo input keys
    curs_set(0); // hide cursor

    // initialize colours
    if(has_colors()) {
        start_color();
        init_pair(1, COLOR_RED,     COLOR_BLACK);
        init_pair(2, COLOR_GREEN,   COLOR_BLACK);
        init_pair(3, COLOR_YELLOW,  COLOR_BLACK);
        init_pair(4, COLOR_BLUE,    COLOR_BLACK);
        init_pair(5, COLOR_MAGENTA, COLOR_BLACK);
        init_pair(6, COLOR_CYAN,    COLOR_BLACK);
        init_pair(7, COLOR_WHITE,   COLOR_BLACK);
    } else {
        log << "Error: Terminal is out of colour ink." << std::endl;
    }

    run_client();

    // terminate ncurses
    endwin();
}
