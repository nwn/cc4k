#include "server/game.hpp"
#include "shared/gamestate.hpp"
#include "shared/floorstate.hpp"
#include "shared/cellstate.hpp"
#include <fstream>
#include <vector>
#include <map>

#include <ncursesw/ncurses.h>

std::ofstream lout("out.log");
std::ofstream lerr("err.log");

void render_map(WINDOW* win, const FloorState& fs, const std::vector<CharacterState>& players) {
    for(std::size_t i = 0; i < fs.cells.size(); ++i) {
        for(std::size_t j = 0; j < fs.cells[i].size(); ++j) {
            const CellState& cs = fs.cells[i][j];

            // determine display character
            chtype c;
            if(cs.entity_id != 0) {
                bool found = false;
                for(std::size_t i = 0; i < players.size(); ++i) {
                    if(players[i].id == cs.entity_id) {
                        c = A_BOLD | COLOR_PAIR(i + 3);
                        found = true;
                        break;
                    }
                }
                if(!found) lerr << "Our hero has gone missing; the world will surely perish." << std::endl;
            }
            else if(cs.type == CellState::Type::Void) c = ' ';
            else if(cs.type == CellState::Type::Wall) c = '#';
            else if(cs.type == CellState::Type::Floor) c = '.';
            else c = '?';

            mvwaddch(win, i, j, c);
        }
    }
}

void render_info(WINDOW* win, const CharacterState& cs) {
    mvwprintw(win, 0, 0, "<Player> [%u,%u,%u]", 0, 0, 0);

    mvwprintw(win, 1, 0, "Health: ");
    attr_t health_highlight = A_BOLD;
    if(3 * cs.hp < cs.max_hp) health_highlight |= COLOR_PAIR(1);
    else if(3 * cs.hp < 2 * cs.max_hp) health_highlight |= COLOR_PAIR(3);
    else health_highlight |= COLOR_PAIR(2);
    wattron(win, health_highlight);
    wprintw(win, "%u/%u", cs.hp, cs.max_hp);
    wattroff(win, health_highlight);

    mvwprintw(win, 2, 0, "Attack: %u", cs.atk);
    mvwprintw(win, 3, 0, "Defence: %u", cs.def);
}

int main() {
    // curses initialization
    {
        initscr(); // initialize curses
        cbreak(); // exit cooked mode
        noecho(); // do not echo input keys
        curs_set(0); // hide cursor

        // initialize colours
        if(has_colors()) {
            start_color();
            init_pair(1, COLOR_RED,     COLOR_BLACK);
            init_pair(2, COLOR_GREEN,   COLOR_BLACK);
            init_pair(3, COLOR_YELLOW,  COLOR_BLACK);
            init_pair(4, COLOR_BLUE,    COLOR_BLACK);
            init_pair(5, COLOR_MAGENTA, COLOR_BLACK);
            init_pair(6, COLOR_CYAN,    COLOR_BLACK);
            init_pair(7, COLOR_WHITE,   COLOR_BLACK);
        } else {
            lout << "Error: Terminal is out of colour ink." << std::endl;
        }
    }

    Game game;
    const GameState gs = game.state();
    std::map<EntityID, const EntityState*> entities;
    for(std::size_t i = 0; i < gs.players.size(); ++i) {
        entities[gs.players[i].id] = &gs.players[i];
    }
    for(std::size_t i = 0; i < gs.players.size(); ++i) {
        entities[gs.players[i].id] = &gs.players[i];
    }

    // render map
    WINDOW* map = newwin(gs.floors.back().cells.size(), gs.floors.back().cells.back().size(), 1, 1);
    if(map == nullptr) {
        lerr << "Error: The cartographer is sleeping." << std::endl;
    }
    render_map(map, gs.floors.back(), gs.players);
    wrefresh(map);
    keypad(map, TRUE); // allow special key input
    wgetch(map);

    // render player info
    WINDOW* info = newwin(8, 16, 1, getmaxx(map) + 2);
    if(info == nullptr) {
        lerr << "Error: Your hero's info is confidential." << std::endl;
    }
    render_info(info, gs.players.back());
    wrefresh(info);
    keypad(info, TRUE); // allow special key input
    wgetch(info);

    delwin(info);
    delwin(map);

    endwin(); // terminate curses
}
